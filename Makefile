#makefile
all:
	cd alarm_dir && make &&  cp Alarm_manager ../ && cp Alarm_manager.conf ../
	cd client_dir && make && cp client ../
	cd manager_dir && make && cp manager libpowers.so ../
clean:
	cd alarm_dir && make clean
	rm Alarm_manager Alarm_manager.conf
	cd client_dir && make clean
	rm client
	cd manager_dir && make clean
	rm manager libpowers.so
