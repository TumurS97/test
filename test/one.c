#include <stdio.h>

#include <time.h>
void main()
{
time_t t;
struct tm *t_m;
t=time(NULL);
t_m=localtime(&t);
//cout<<"Local time is: "<<t_m->tm_hour<<":"<<t_m->tm_min<<":"<<t_m->tm_sec;

printf("[%d-%d-%d %d:%d:%d]\n",1900 + t_m->tm_year,t_m->tm_mon,
	t_m->tm_mday,t_m->tm_hour,t_m->tm_min,t_m->tm_sec );
}