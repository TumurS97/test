#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <syslog.h>
#include "zmq.h"
#include <zlog.h>
#include "zlog.h"
#include <pthread.h> 
#include <assert.h>

#define ALARM 1
#define NORMILIZE 0
#define MESSAGE_LEN 124
#define N 20          /*Размер очереди сообщений */

#define DELETE_ALL 1
#define READ_ALL 2

#define ACK 6


typedef struct my_message
{
	pid_t pid;
	unsigned int priority;
	unsigned int type;
	char mes[MESSAGE_LEN];
} my_message;

void Create_message( my_message *arg,char *string);
void *Create_Recieve_fun(void *args);
void *Create_Manager_fun(void *args);
void *Create_log_fun(void *args);

