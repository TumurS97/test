#include "myDemon.h"
#define DELETE_ALL 1
#define READ_ALL 2
#define FILTR 3
#define FILTR_MODULE 4
#define FILTR_PRIORITY 5
#define FILTR_ACTIVE 6

int rcv_mesage(my_message *arg, void* socket);
int search_active_alarm(my_message *arg, int size);

int delete(void *requester)
{
	int *buffer = NULL;
	int *num = NULL;
	buffer= (int*)malloc(sizeof(int));
	num = (int*)malloc(sizeof(int));
	*num = DELETE_ALL;
	zmq_send (requester, num, sizeof(num), 0);
	zmq_recv (requester, buffer, sizeof(buffer), 0);
    printf("%d\n", *buffer);
    free(num);
    free(buffer);
	return 0;
};

int write_all(void *requester)
{
	my_message args[N]={0};
	int size_mes = rcv_mesage(args,requester);

	for(int i=0;i<size_mes;i++)
	printf("[%d|%d|%d|%s]\n",args[i].pid,args[i].priority,
							args[i].type,args[i].mes );
	return 0;
};


int write_by_filt(void *requester, int fild,int value)
{
	my_message args[N]={0};
	int size_mes = rcv_mesage(args,requester);

	if(fild == FILTR_MODULE)
	{
		for(int i=0;i<size_mes;i++)
		{
			if(args[i].pid == value)
			{
				printf("[%d|%d|%d|%s]\n",args[i].pid,args[i].priority,
							args[i].type,args[i].mes );
				fflush(stdout);
			}
		}
	}

	if(fild == FILTR_PRIORITY)
	{
		for(int i=0;i<size_mes;i++)
		{
			if(args[i].priority == value)
			{
				printf("[%d|%d|%d|%s]\n",args[i].pid,args[i].priority,
							args[i].type,args[i].mes );
			fflush(stdout);
			}
		}
	}

	if(fild == FILTR_ACTIVE)
	{
		search_active_alarm(args,size_mes);
	}


	return 0;
}


int search_active_alarm(my_message *args, int size)
{
	for (int i=0;i<size;i++)
	{
		if (args[i].type == ALARM)
		{
			for(int j=0;j<size;j++)
			{
				if( i != j && args[i].pid == args[j].pid  &&
				 	args[j].type != NORMILIZE && 
				 	strcmp(args[i].mes,args[j].mes) == 0)
				{
					printf("[%d|%d|%d|%s]\n",args[i].pid,args[i].priority,
											args[i].type,args[i].mes );
					break;
				}
				
			}
		}
	}
}



/*Принимает сообщения от alarm manager */
int rcv_mesage(my_message *arg,void* socket)
{
	int *num = NULL;
	num = (int*)malloc(sizeof(int));
	*num = READ_ALL;
	zmq_send (socket, num, sizeof(num), 0);
	free(num);

	int64_t more=0;
	size_t more_size = sizeof(more);

	int i=0;
	do
	{
		zmq_recv(socket,&arg[i],sizeof(my_message),0);
		zmq_getsockopt(socket,ZMQ_RCVMORE,&more,&more_size);
		i++;

	}while(more);
	
	return i;
}
