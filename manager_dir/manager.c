
#include "myDemon.h"
#include <dlfcn.h>

#define FUN_DELETE  "delete"
#define FUN_WRITE_ALL "write_all"
#define TCP_PORT  "tcp://localhost:5555"
#define LIB_POWER "./libpowers.so"
#define FUN_WRITE_BY_FILTR "write_by_filt"

#define FILTR 3
#define FILTR_MODULE 4
#define FILTR_PRIORITY 5
#define FILTR_ACTIVE 6

int main (int argc, char *argv[])
{
    void *context = zmq_ctx_new ();
    void *requester = zmq_socket (context, ZMQ_REQ);
    int conn = zmq_connect (requester, TCP_PORT);

    void *ext_library;  
	int (*powerfunc)(void *requester);
    int (*filtrfunc)(void * requester, int fild,int value);

    int input = 0;
    int fild = 0;
    int value = 0;

	printf("Action: delete all [1] / read all [2] / read by filtr [3]\nEnter: ");
	
  	scanf( "%d", &input );
    if(input == FILTR)
    {
        printf("Filtr by fild: module [4] | priority [5] | active alarm [6]\nEnter: ");
        scanf("%d",&fild);

        if(fild != FILTR_MODULE &&  fild != FILTR_PRIORITY && fild !=FILTR_ACTIVE )
        {
            printf( "Incorrect input1\n" );
            goto target;
        }

        
        if(fild == FILTR_PRIORITY ||  fild == FILTR_MODULE)
        {
            printf("Enter value: ");
            scanf("%d",&value);
        }

       

    }

    switch ( input ) 
    {
        case 1:  /*Удаляет все сообщения из очереди */

          	ext_library = dlopen(LIB_POWER, RTLD_LAZY);
          	if (!ext_library)
			{
				fprintf(stderr,"dlopen() error: %s\n", dlerror());
				goto target;
			}

			powerfunc = dlsym(ext_library, FUN_DELETE);
			printf("Код:  %d\n",(*powerfunc)(requester));
			dlclose(ext_library);
            goto target;

        case 2: /*Вычитывает все сообщения*/

        	ext_library = dlopen(LIB_POWER, RTLD_LAZY);
        	if (!ext_library)
			{
				fprintf(stderr,"dlopen() error: %s\n", dlerror());
				goto target;
			}

			powerfunc = dlsym(ext_library, FUN_WRITE_ALL);
			printf("Код:  %d\n",(*powerfunc)(requester));
			dlclose(ext_library);
            goto target;

        case 3: /*Вычитывает все сообщение по соответствующему фильтру */

            ext_library = dlopen(LIB_POWER, RTLD_LAZY);
            if (!ext_library)
            {
                fprintf(stderr,"dlopen() error: %s\n", dlerror());
                goto target;
            }

            filtrfunc = dlsym(ext_library, FUN_WRITE_BY_FILTR);
            printf("Код:  %d\n",(*filtrfunc)(requester,fild,value));
            dlclose(ext_library);
            goto target;


        default:
            printf( "Incorrect input2\n" );
            goto target;
    }


        	
target:
    zmq_close (requester);
    zmq_ctx_destroy (context);
    return 0;
}
